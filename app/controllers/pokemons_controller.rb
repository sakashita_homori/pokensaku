class PokemonsController < ApplicationController
  before_action :set_pokemon, only: [:show, :edit, :update, :destroy]

  def index
    @p = Pokemon.ransack(params[:p])
    @pokemons = @p.result(distinct: true)
  end

 private
 
    def set_pokemon
      @pokemon = Pokemon.find(params[:id])
    end

    def pokemon_params
      params.require(:pokemon).permit(:name, :nom, :type1, :type2, :hei, :weignt)
    end
end