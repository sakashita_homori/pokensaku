json.extract! pokemon, :id, :name, :nom, :type1, :type2, :hei, :weight, :created_at, :updated_at
json.url pokemon_url(pokemon, format: :json)
