Rails.application.routes.draw do
  resources :pokemons
  get 'pokemons/index'
  root 'pokemons#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
